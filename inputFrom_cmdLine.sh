#!/usr/bin/bash

############### ASSIGNING VARIABLES WHICH WILL GET THE INPUTS FROM THE COMMAND LINE #############

echo "Variables are declared to get inputs from command line"

a=$1
b=$2
c=$((a + b))

echo "the value of first variable A is" ${a}
echo "the vaule of second varibale B is" ${b}

echo "Just to verify both values are correctly configured, we are providing the addition of two values ------>" ${c}


############################## END OF THE SCRIPT ################################################
