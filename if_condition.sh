#!/usr/bin/bash


############# INITIALIZING VARIABLE FROM THE USER USING READ COMMAND ############################


read -p "Enter the ID of the user ------>" num

echo "The ID of the user should not be zero. we are validating this condition using IF statements by assigning additional variable as Zero value"

a=0

if [ ${num} == ${a} ]; then
	echo "Both Values are Equal. User entered invalid ID. User ID should not be $num"
	echo "ENTERED VALUE DOESNT STATISFIED OUR CONDITION"
elif [ ${num} != ${a} ]; then
	echo "Both values are not Equal. User entered Valid ID."
fi

echo "CONDITIONS STATEMENTS ARE COMPLETED"

################################### END OF THE SCRIPT #####################
