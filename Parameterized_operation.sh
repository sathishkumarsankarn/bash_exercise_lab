#!/usr/bin/bash

############### PASSING THE VARIABLES AS INPUT PARAMETERS WHILE EXECUTING THE SCRIPT ########

num1=$1
num2=$2

echo "Inputs values are passed as a parameter while executing the script itself."

echo "Now performing the operation to get the reminder of the two variables using dividing it."

echo "REMAINDER OF TWO VARIABLES: $((num1 % num2))"

################################ END OF THE SCRIPT ########################################
