#!/usr/bin/bash

######################### VARIABLE DECLARATION #######################

read -p "Getting First variable values from the user------> " num1

read -p "Getting Second Variable value from the user------> " num2

##################### SUM OF TWO VARIABLES OPERATION #################

echo "SUM OF TWO VARIBALES: $((num1 + num2))"

##################### SUBTRACTION OF TWO VARIABLES OPERATION ##########

echo "DIFFERENCE OF TWO VARIABLES: $((num1 - num2))"

##################### MULTIPLICATION OF TWO VARIABLES OPERATION ########

echo "MULTIPLICATION OF TWO VARIABLES: $((num1 * num2))"

#################### DIVISION OF TWO VARIABLES OPERATION ###############

echo "DIVISION OF TWO VARIABLES: $((num1 / num2))"

############################## END OF THE SCRIPT ######################

