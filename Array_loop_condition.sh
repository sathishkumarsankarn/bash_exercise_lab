#!/usr/bin/bash

############# INITIALIZING ARRAY VARIABLE WITH VALUES ###################

a=(11 12 13 14 15)

######## DECLARING LOOP STATEMENT TO PRINT ARRAY VALUES ################

for i in ${a[@]}
do
	echo "The value of Array Variable A in each loop is" ${i}


done

################### END OF THE SCRIPT ##################################
