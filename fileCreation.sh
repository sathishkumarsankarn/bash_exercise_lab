#!/usr/bin/bash

##################### VARIABLE DECLARATION #################

read -p "Enter the name of the file which should be created from this script ----> " name

touch ${name}.txt

echo "The file is created successfully. Now adding content to the created file"

echo "This file is created while executing filecreation.sh script and it is auto-generated file" >> ${name}.txt

############################### END OF THE SCRIPT ###########################
